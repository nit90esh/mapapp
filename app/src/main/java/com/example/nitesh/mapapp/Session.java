package com.example.nitesh.mapapp;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.util.HashMap;

public class Session {
    SharedPreferences sharedPreferences;
    Editor editor;
    Context context;
    int MODE = 0;
    private static final String PREFER_NAME = "locSession";
    private static final String IS_STARTED = "isStarted";
    private static final String START_LOC = "startLoc";
    private static final String STOP_LOC = "stopLoc";

    public Session(Context context){
        this.context = context;
        sharedPreferences = this.context.getSharedPreferences(PREFER_NAME, MODE);
        editor = sharedPreferences.edit();
    }
    public void createStartSession(LatLng latLng){
        editor.clear().commit();
        editor.putBoolean(IS_STARTED, true);
        editor.putString(START_LOC,latLng.toString());
        editor.commit();
    }
    public void createStopSession(LatLng latLng){
        editor.putBoolean(IS_STARTED, false);
        editor.putString(STOP_LOC,latLng.toString());
        editor.commit();
    }
    public String getStartSession(){
        return sharedPreferences.getString(START_LOC,"");
    }
    public String getStopSession(){
        return sharedPreferences.getString(STOP_LOC,"");
    }
    public boolean checkStarted(){
        if(!sharedPreferences.getBoolean(IS_STARTED, false))
            return true;
        return false;
    }
}